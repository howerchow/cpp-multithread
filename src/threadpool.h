#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <any>
#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <semaphore>
#include <thread>
#include <unordered_map>
#include <vector>

// 实现一个信号量类
class Semaphore
{
      public:
        Semaphore( int limit = 0 ) : resLimit_( limit ) {}
        ~Semaphore() = default;

        // 获取一个信号量资源
        void wait()
        {
                std::unique_lock lock { mtx_ };
                // 等待信号量有资源，没有资源的话，会阻塞当前线程
                cond_.wait( lock, [ & ]() -> bool { return resLimit_ > 0; } );
                --resLimit_;
        }

        // 增加一个信号量资源
        void post()
        {
                std::unique_lock lock { mtx_ };
                ++resLimit_;
                // linux下condition_variable的析构函数什么也没做
                // 导致这里状态已经失效，无故阻塞
                cond_.notify_all(); // 等待状态，释放mutex锁 通知条件变量wait的地方，可以起来干活了
        }

      private:
        int resLimit_;
        std::mutex mtx_;
        std::condition_variable cond_;
};

// Task类型的前置声明
class Task;

// 实现接收提交到线程池的task任务执行完成后的返回值类型Result
class Result
{
      public:
        Result( std::shared_ptr<Task> task, bool isValid = true );
        ~Result() = default;

        // 问题一：setVal方法，获取任务执行完的返回值的
        void setVal( std::any any );

        // 问题二：get方法，用户调用这个方法获取task的返回值
        std::any get();

      private:
        std::any any_;               // 存储任务的返回值
        Semaphore sem_;              // 线程通信信号量
        std::shared_ptr<Task> task_; // 指向对应获取返回值的任务对象
        std::atomic_bool isValid_;   // 返回值是否有效
};

// 任务抽象基类
class Task
{
      public:
        Task();
        virtual ~Task() = default;
        void exec();
        void setResult( Result* res );

        // 用户可以自定义任意任务类型，从Task继承，重写run方法，实现自定义任务处理
        virtual std::any run() = 0;

      private:
        Result* result_; // Result对象的声明周期 》 Task的
};

// 线程池支持的模式
enum class PoolMode
{
        MODE_FIXED,  // 固定数量的线程
        MODE_CACHED, // 线程数量可动态增长
};

// 线程类型
class Thread
{
      public:
        using ThreadFunc = std::function<void( int )>;

        Thread( ThreadFunc func );
        ~Thread();
        void start();
        int getId() const;

      private:
        ThreadFunc func_;
        static int generateId_;
        int threadId_; // 保存线程id
};

/*
example:
ThreadPool pool;
pool.start(4);

class MyTask : public Task
{
    public:
        void run() { // 线程代码... }
};

pool.submitTask(std::make_shared<MyTask>());
*/
// 线程池类型
class ThreadPool
{
      public:
        ThreadPool();
        ~ThreadPool();
        ThreadPool( const ThreadPool& ) = delete;
        ThreadPool& operator=( const ThreadPool& ) = delete;

        // 设置线程池的工作模式
        void setMode( PoolMode mode );

        // 设置task任务队列上线阈值
        void setTaskQueMaxThreshHold( int threshhold );

        // 设置线程池cached模式下线程阈值
        void setThreadSizeThreshHold( int threshhold );

        // 给线程池提交任务
        Result submitTask( std::shared_ptr<Task> sp );

        // 开启线程池
        void start( int initThreadSize = std::thread::hardware_concurrency() );

      private:
        // 定义线程函数
        void threadFunc( int threadid );

        // 检查pool的运行状态
        bool checkRunningState() const;

      private:
        // std::vector<std::unique_ptr<Thread>> threads_; // 线程列表
        std::unordered_map<int, std::unique_ptr<Thread>> threads_; // 线程列表

        int initThreadSize_;             // 初始的线程数量
        int threadSizeThreshHold_;       // 线程数量上限阈值
        std::atomic_int curThreadSize_;  // 记录当前线程池里面线程的总数量
        std::atomic_int idleThreadSize_; // 记录空闲线程的数量

        std::queue<std::shared_ptr<Task>> taskQue_; // 任务队列
        std::atomic_int taskSize_;                  // 任务的数量
        int taskQueMaxThreshHold_;                  // 任务队列数量上限阈值

        std::mutex taskQueMtx_;            // 保证任务队列的线程安全
        std::condition_variable notFull_;  // 表示任务队列不满
        std::condition_variable notEmpty_; // 表示任务队列不空
        std::condition_variable exitCond_; // 等到线程资源全部回收

        PoolMode poolMode_;              // 当前线程池的工作模式
        std::atomic_bool isPoolRunning_; // 表示当前线程池的启动状态
};

#endif
